package com.android4you.smartvisiontask

import android.app.Application
import com.android4you.smartvisiontask.di.component.DaggerApplicationComponent
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasAndroidInjector
import javax.inject.Inject

class MyApp: Application()

    , HasAndroidInjector {

        @Inject
        lateinit var androidInjector: DispatchingAndroidInjector<Any>

        override fun onCreate() {
            super.onCreate()

            DaggerApplicationComponent.builder().create(this).build().inject(this)
        }

        override fun androidInjector(): AndroidInjector<Any> = androidInjector

    }
