package com.android4you.smartvisiontask.data.model

import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.android.parcel.Parcelize

@Parcelize
@Entity(tableName = "locations")
data class Cordinate(
    @PrimaryKey
    val id: String,
    val lat: String,
    val loc: String,
    val lon: String,
    val office: String,
    val radius: Int,
    var distance: Double?
):Parcelable

