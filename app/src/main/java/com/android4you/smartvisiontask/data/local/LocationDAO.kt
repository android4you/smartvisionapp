package com.android4you.smartvisiontask.data.local

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.android4you.smartvisiontask.data.model.Cordinate


@Dao
interface LocationDAO {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertall(userEntity: List<Cordinate>)



    @Query("SELECT * FROM locations")
    fun getAllLocation(): List<Cordinate>
}
