package com.android4you.smartvisiontask.data.repository

import android.location.Location
import com.android4you.smartvisiontask.data.local.LocationDAO
import com.android4you.smartvisiontask.data.model.Cordinate
import com.android4you.smartvisiontask.data.model.Resource
import com.android4you.smartvisiontask.data.remote.ApiService
import javax.inject.Inject

class LocationRepository @Inject constructor(
    private val apiService: ApiService,
    private val locationDao: LocationDAO
) {

    suspend fun getLocationInfo(
        query: String,
        currentLat: Double?,
        currentLong: Double?,
        isOnline: Boolean
    ): Resource<List<Cordinate>> {
        return if (isOnline) {
            getLocationRemoteInfo(query, currentLat, currentLong)
        } else {
            getLocationLocalInfo(query, currentLat, currentLong)
        }

    }

    suspend fun getLocationRemoteInfo(
        query: String,
        currentLat: Double?,
        currentLong: Double?
    ): Resource<List<Cordinate>> {
        return try {
            val response = apiService.getLocationInfo()
            if (response.isSuccessful) {
                val list = response.body()?.Cordinates
                locationDao.insertall(list!!)

                if (list.isNotEmpty()) {
                    val filteredList = list.filter {
                        it.distance = distanceformula(
                            currentLat!!,
                            currentLong!!,
                            it.lat.toDouble(),
                            it.lon.toDouble()
                        ).toDouble()
                        it.loc.contains(query, true) || it.office.contains(query, true)
                    }
                    val sortedList = filteredList.sortedWith(compareBy({ it.distance }))
                    Resource.Success(sortedList)
                } else {
                    Resource.Success(list)
                }

            } else {
                Resource.Error(Exception(response.message()))

            }
        } catch (ex: Exception) {
            return Resource.Error(ex)
        }

    }

    suspend fun getLocationLocalInfo(
        query: String,
        currentLat: Double?,
        currentLong: Double?
    ): Resource<List<Cordinate>> {
        return try {
            val list = locationDao.getAllLocation()
            if (list.isNotEmpty()) {
                val filteredList = list.filter {
                    it.distance = distanceformula(
                        currentLat!!,
                        currentLong!!,
                        it.lat.toDouble(),
                        it.lon.toDouble()
                    ).toDouble()
                    it.loc.contains(query, true) || it.office.contains(query, true)
                }
                val sortedList = filteredList.sortedWith(compareBy({ it.distance }))
                Resource.Success(sortedList)
            } else {
                Resource.Success(list)
            }
        } catch (ex: Exception) {
            return Resource.Error(ex)
        }

    }

    private fun distanceformula(
        lat1: Double,
        lon1: Double,
        lat2: Double,
        lon2: Double
    ): Float {
        val loc1 = Location("")
        loc1.setLatitude(lat1)
        loc1.setLongitude(lon1)
        val loc2 = Location("")
        loc2.setLatitude(lat2)
        loc2.setLongitude(lon2)
        val distanceInMeters: Float = loc1.distanceTo(loc2)
        val dk = distanceInMeters / 1000
        return dk
    }


}