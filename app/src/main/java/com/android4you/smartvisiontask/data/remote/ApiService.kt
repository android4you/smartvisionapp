package com.android4you.smartvisiontask.data.remote

import com.android4you.smartvisiontask.data.model.LocationInfoModel
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query


interface ApiService {

    @GET("LocationsApp.json")
    suspend fun getLocationInfo(): Response<LocationInfoModel>


}
