package com.android4you.smartvisiontask.data.model

data class LocationInfoModel(
    val Cordinates: List<Cordinate>
)