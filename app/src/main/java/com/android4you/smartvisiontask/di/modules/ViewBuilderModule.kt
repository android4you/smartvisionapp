package com.android4you.smartvisiontask.di.modules

import com.android4you.smartvisiontask.ui.MainActivity
import com.android4you.smartvisiontask.ui.location.LocationListFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ViewBuilderModule {

    @ContributesAndroidInjector
    abstract fun bindMainActivity(): MainActivity


    @ContributesAndroidInjector
    abstract fun bindHomeFragment(): LocationListFragment

}
