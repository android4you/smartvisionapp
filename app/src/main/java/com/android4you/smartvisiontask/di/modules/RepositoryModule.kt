package com.android4you.smartvisiontask.di.modules

import com.android4you.smartvisiontask.data.local.LocationDAO
import com.android4you.smartvisiontask.data.remote.ApiService
import com.android4you.smartvisiontask.data.repository.LocationRepository
import dagger.Module
import dagger.Provides

@Module
class RepositoryModule {

    @Provides
    fun provideApiRepository(
        apiService: ApiService,
        locationDao: LocationDAO

    ): LocationRepository {
        return LocationRepository(apiService, locationDao)
    }
}
