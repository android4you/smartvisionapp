package com.android4you.smartvisiontask.di.modules

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.android4you.smartvisiontask.ui.location.ShareViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap


@Module
abstract class ViewModelModule {

    /**
     * Binds ViewModelFactory to instantiate ViewModels
     */
    @Binds
    internal abstract fun bindViewModelFactory(viewModelFactory: ViewModelFactory): ViewModelProvider.Factory

    @Binds
    @IntoMap
    @ViewModelKey(ShareViewModel::class)
    internal abstract fun bindApiViewModel(viewModel: ShareViewModel): ViewModel




}
