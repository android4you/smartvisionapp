package com.android4you.smartvisiontask.di.component

import android.app.Application
import android.content.Context
import com.android4you.smartvisiontask.MyApp
import com.android4you.smartvisiontask.di.modules.*
import com.android4you.smartvisiontask.ui.location.LocationListFragment
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import dagger.android.AndroidInjector
import javax.inject.Singleton


@Singleton
@Component(
    modules = [AndroidInjectionModule::class, ViewBuilderModule::class,
        ViewModelModule::class, RepositoryModule::class,
        NetworkModule::class, DatabaseModule::class]
)
interface ApplicationComponent : AndroidInjector<MyApp> {


    @Component.Builder
    interface Builder {

        @BindsInstance
        fun create(application: Application): Builder

        fun build(): ApplicationComponent
    }

    override fun inject(instance: MyApp)

    fun inject(fragment: LocationListFragment)


}
