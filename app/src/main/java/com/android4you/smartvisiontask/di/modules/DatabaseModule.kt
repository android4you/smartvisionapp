package com.android4you.smartvisiontask.di.modules

import android.app.Application
import com.android4you.smartvisiontask.data.local.AppDatabase
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class DatabaseModule {

    @Singleton
    @Provides
    fun providesDatabase(application: Application) = AppDatabase.invoke(application)

    @Singleton
    @Provides
    fun providesLocationDao(database: AppDatabase) = database.locationDAO()
}
