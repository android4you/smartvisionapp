package com.android4you.smartvisiontask.ui.location

import android.Manifest
import android.annotation.SuppressLint
import android.content.Context
import android.content.pm.PackageManager
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.os.Bundle
import android.os.Looper
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.widget.addTextChangedListener
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.android4you.smartvisiontask.MyApp
import com.android4you.smartvisiontask.R
import com.android4you.smartvisiontask.data.model.Cordinate
import com.android4you.smartvisiontask.data.model.Resource
import com.android4you.smartvisiontask.di.modules.ViewModelFactory
import com.android4you.smartvisiontask.extension.makeGone
import com.android4you.smartvisiontask.extension.makeVisible
import com.android4you.smartvisiontask.utils.PermissionUtils
import com.google.android.gms.location.LocationCallback
import kotlinx.android.synthetic.main.fragment_list_location.*
import javax.inject.Inject
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationResult
import com.google.android.gms.location.LocationServices

class LocationListFragment : Fragment(R.layout.fragment_list_location) , OnItemClickListener{

    lateinit var viewModel: ShareViewModel

    @Inject
    lateinit var viewModelFactory: ViewModelFactory


    private var currentLat: Double? =null //25.239462515208178
    private var currentLong: Double? =null // 55.30397071074165


    companion object {
        private const val LOCATION_PERMISSION_REQUEST_CODE = 999
    }


    override fun onAttach(context: Context) {
        super.onAttach(context)
        val component = (requireActivity().application as MyApp).androidInjector
        component.inject(this)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = ViewModelProvider(this, viewModelFactory)[ShareViewModel::class.java]
        fetchLocation()
    }

    private fun fetchLocation() {
        search_boxTV.addTextChangedListener {
                viewModel.setSearchParameter(it.toString())
        }
    }

    private fun setUpLocationListener() {
        val fusedLocationProviderClient =
            LocationServices.getFusedLocationProviderClient(requireContext())
        val locationRequest = LocationRequest().setInterval(200000).setFastestInterval(2000)
            .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
        if (ActivityCompat.checkSelfPermission(
                requireContext(),
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                requireContext(),
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            return
        }
        fusedLocationProviderClient.requestLocationUpdates(
            locationRequest,
            object : LocationCallback() {
                @SuppressLint("MissingPermission")
                override fun onLocationResult(locationResult: LocationResult) {
                    super.onLocationResult(locationResult)
                    for (location in locationResult.locations) {
                        currentLat = location.latitude
                        currentLong = location.longitude
                    }

                }
            },
            Looper.getMainLooper()
        )

        fusedLocationProviderClient.lastLocation.addOnSuccessListener {location ->
            currentLat = location.latitude
            currentLong = location.longitude
            viewModel.getLocationInfoLiveData(currentLat, currentLong, checkNetwork())
                .observe(viewLifecycleOwner, Observer { resource ->
                    if (resource is Resource.Loading) {
                        progressBar.makeVisible()
                    } else progressBar.makeGone()

                    when (resource) {
                        is Resource.Error -> {
                            showErrorMessage(resource.exception.message)
                        }
                        is Resource.Success -> {
                            if(resource.data!!.isNotEmpty())
                                showLocationList(resource.data)
                            else
                                showErrorMessage(getString(R.string.empty))
                        }
                        else -> {
                        }
                    }
                })
        }
    }

    override fun onStart() {
        super.onStart()
        when {
            PermissionUtils.isAccessFineLocationGranted(requireContext()) -> {
                when {
                    PermissionUtils.isLocationEnabled(requireContext()) -> {
                        setUpLocationListener()
                    }
                    else -> {
                        PermissionUtils.showGPSNotEnabledDialog(requireContext())
                    }
                }
            }
            else -> {
                PermissionUtils.requestAccessFineLocationPermission(
                    requireActivity() as AppCompatActivity,
                    LOCATION_PERMISSION_REQUEST_CODE
                )
            }
        }
    }

    override fun onPause() {
        super.onPause()
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            LOCATION_PERMISSION_REQUEST_CODE -> {
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    when {
                        PermissionUtils.isLocationEnabled(requireContext()) -> {
                            setUpLocationListener()
                        }
                        else -> {
                            PermissionUtils.showGPSNotEnabledDialog(requireContext())
                        }
                    }
                } else {
                    Toast.makeText(
                        requireContext(),
                        getString(R.string.location_permission_not_granted),
                        Toast.LENGTH_LONG
                    ).show()
                }
            }
        }
    }

    private fun showErrorMessage(msg: String?) {
        errorTV.text = msg ?: "Error"//getString(R.string.something_went_wrong)
        recyclerView.makeGone()
        errorTV.makeVisible()
    }

    private fun showLocationList(list: List<Cordinate>?) {
        list?.let {
            recyclerView.makeVisible()
            errorTV.makeGone()
            val adapter = LocationListAdapter(requireActivity(), it, this)
            recyclerView.adapter = adapter
        }

    }

    override fun onStop() {
        super.onStop()

    }

    override fun onItemClick(model: Cordinate) {
        val action = LocationListFragmentDirections.actionLocationListFragmentToDetailsFragment(model)
        findNavController().navigate(action)

    }

    private fun checkNetwork(): Boolean {
        val cm = requireActivity().getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetwork: NetworkInfo? = cm.activeNetworkInfo
        val isConnected: Boolean = activeNetwork?.isConnectedOrConnecting == true

        return isConnected
    }
}