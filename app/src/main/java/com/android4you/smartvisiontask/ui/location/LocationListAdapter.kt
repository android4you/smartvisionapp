package com.android4you.smartvisiontask.ui.location

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.android4you.smartvisiontask.R
import com.android4you.smartvisiontask.data.model.Cordinate

class LocationListAdapter (private val context: Context, private val dataList: List<Cordinate>, private val listener: OnItemClickListener) :
        RecyclerView.Adapter<LocationListAdapter.ViewHolder>() {

    override fun getItemCount(): Int {
        return dataList.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val itemView = LayoutInflater.from(context).inflate(R.layout.item_location, parent, false)
        return ViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val model = dataList[position]
        model.apply {
            holder.locationNameTV.text =  this.loc.plus(" - ").plus(this.office)// this.loc +" - "+ this.office
            holder.latlngTV.text = this.lat+ " : "+this.lon
            holder.distanceTV.text =  "%.2f".format(this.distance).toString()+" Km"
        }

        holder.rootView.setOnClickListener {
            listener.onItemClick(model)
        }
    }


    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val locationNameTV: TextView = itemView.findViewById(R.id.location_nameTV)
        val latlngTV: TextView = itemView.findViewById(R.id.latitudelongitude_tv)
        val distanceTV: TextView = itemView.findViewById(R.id.distance_tv)
        val rootView : LinearLayout = itemView.findViewById(R.id.rootView)

    }

}


interface OnItemClickListener{
    fun onItemClick(model : Cordinate)
}