package com.android4you.smartvisiontask.ui.details

import android.graphics.Color
import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.navArgs
import com.android4you.smartvisiontask.R
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.CircleOptions
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import java.util.*


class DetailsFragment : Fragment(R.layout.fragment_details), OnMapReadyCallback {

    private lateinit var mMap: GoogleMap

    private val arg: DetailsFragmentArgs by navArgs()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val mapFragment = childFragmentManager
            .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
    }

    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap
        startDemo(false)
    }

    protected fun startDemo(isRestore: Boolean) {
        if (!isRestore) {
           val cordinate =  arg.Cordinate
            val latLng = LatLng(
                cordinate.lat.toDouble(),
                cordinate.lon.toDouble()
            )
            mMap.moveCamera(
                CameraUpdateFactory.newLatLngZoom(
                    latLng,
                    18f
                )
            )
            val snippet = String.format(
                Locale.getDefault(),
                "Lat: %1$.5f, Long: %2$.5f",
                latLng.latitude,
                latLng.longitude
            )
            mMap.addMarker(
                MarkerOptions().position(
                    latLng
                ).title(arg.Cordinate.office.plus(" - ").plus(arg.Cordinate.loc))
                    .snippet(snippet)
            )
            mMap.setOnInfoWindowClickListener {
                drawCircle(latLng, cordinate.radius.toDouble())
            }

        }
    }
    private fun drawCircle(point: LatLng, radius:Double) {
        val circleOptions = CircleOptions()
        circleOptions.center(point)
        circleOptions.radius(radius)
        circleOptions.strokeColor(Color.BLACK)
        circleOptions.fillColor(0x30ff0000)
        circleOptions.strokeWidth(2f)
        mMap.addCircle(circleOptions)
    }
}