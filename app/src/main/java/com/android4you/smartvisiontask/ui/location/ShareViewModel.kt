package com.android4you.smartvisiontask.ui.location

import androidx.lifecycle.*
import com.android4you.smartvisiontask.data.model.Cordinate
import com.android4you.smartvisiontask.data.model.Resource
import com.android4you.smartvisiontask.data.repository.LocationRepository
import kotlinx.coroutines.Dispatchers
import javax.inject.Inject

class ShareViewModel @Inject constructor(
    private val locationRepository: LocationRepository
) : ViewModel() {

    private val searchQueryLiveData: MutableLiveData<String> = MutableLiveData("")

    fun setSearchParameter(searchParam: String) {
        searchQueryLiveData.setValue(searchParam)
    }

    fun getLocationInfoLiveData(currentLat: Double?, currentLong: Double?, isOnlne:Boolean): LiveData<Resource<List<Cordinate>>> {
        return Transformations.switchMap(
            searchQueryLiveData
        ) { searchParam ->
            liveData(context = viewModelScope.coroutineContext + Dispatchers.IO) {
                emit(Resource.Loading())
                emit(locationRepository.getLocationInfo( searchParam, currentLat, currentLong, isOnlne))
            }
        }
    }

//
//    fun getLocationInfo() =
//        liveData(context = viewModelScope.coroutineContext + Dispatchers.IO) {
//            emit(Resource.Loading())
//            emit(locationRepository.getLocationInfo())
//        }


}